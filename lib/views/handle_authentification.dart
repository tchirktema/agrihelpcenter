//import 'package:doonenyo/services/shared_preference_service.dart';
//import 'package:doonenyo/views/404/not_found_screen.dart';

//import 'package:doonenyo/views/splashscreen/intro_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:lorofe/views/home/home_screen.dart';
import 'package:lorofe/views/intro_screen.dart';

class HandleAuthentification extends StatefulWidget {
  @override
  _HandleAuthentificationState createState() => _HandleAuthentificationState();
}

class _HandleAuthentificationState extends State<HandleAuthentification> {
 
 @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new StreamBuilder<FirebaseUser>(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, snapshot){
          if(snapshot.hasData){
            return HomeScreen();
          }else{
            return IntroScreen();
          }
        }
    );
  }
}
