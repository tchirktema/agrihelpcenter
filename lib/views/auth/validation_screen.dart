import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:lorofe/config/asset_tools.dart';

class ValidationScreen extends StatefulWidget {
  final String verificationId;
  final String phoneNumber;
  ValidationScreen({this.verificationId,this.phoneNumber});
  @override
  _ValidationScreenState createState() => _ValidationScreenState();
}

class _ValidationScreenState extends State<ValidationScreen> {
  TextEditingController smsController;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String _message;

  @override
  void initState() {
    super.initState();
    this.smsController = new TextEditingController();

  }


  Future<void> _signInWithPhoneNumber(String smsCode) async {

    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: widget.verificationId,
      smsCode: smsController.text,
    );
    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    setState(() {
      if (user != null) {
        _message = 'Successfully signed in, uid: ' + user.uid;

        Navigator.of(context)
            .pushNamedAndRemoveUntil(AssetTools.rtHomeScreen, (Route<dynamic> route) => false);

      } else {
        _message = 'Sign in failed';
      }
    });

  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        padding: const EdgeInsets.all(16.0),
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.lightGreen,
              Colors.green
            ]
          )
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 40.0,bottom: 20.0),
              height: 80,
              child: Image.asset(AssetTools.imageLogo)
            ),
            Text("Good In Food".toUpperCase(), 
              style: TextStyle(
                color: Colors.white70,
                fontSize: 24.0,
                fontWeight: FontWeight.bold
              ),
            ),
            Text("Veulliez entrer le code recu par sms".toUpperCase(), 
              style: TextStyle(
                color: Colors.white70,
                fontSize: 10.0,
                fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: 40.0),
            TextField(
              controller: smsController,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(16.0),
                prefixIcon: Container(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                  margin: const EdgeInsets.only(right: 8.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      bottomLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0),
                      bottomRight: Radius.circular(10.0)
                    )
                  ),
                  child: Icon(Icons.smartphone, color: Colors.lightGreen,)
                ),
                hintText: "Veulliez entrer le code",
                hintStyle: TextStyle(color: Colors.white54),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  borderSide: BorderSide.none
                ),
                filled: true,
                fillColor: Colors.white.withOpacity(0.1),
              ),
              
            ),
            
            SizedBox(height: 20.0),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                color: Colors.white,
                textColor: Colors.lightGreen,
                padding: const EdgeInsets.all(20.0),
                child: Text("Valider".toUpperCase()),
                onPressed: (){
                  if(smsController.text.isNotEmpty){
                    _signInWithPhoneNumber(smsController.text);
                  } else{
                    final snackBar = SnackBar(
                      content: Text('Code sms Invalide'),
                      action: SnackBarAction(
                        label: 'ok',
                        onPressed: () {
                          // Some code to undo the change.
                        },
                      ),
                    );
                    Scaffold.of(context).showSnackBar(snackBar);
                  }
                },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)
                ),
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}