import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:lorofe/config/asset_tools.dart';
import 'package:latlong/latlong.dart';
import 'package:lorofe/views/home/marche_screen.dart';
import 'package:lorofe/widgets/data_search.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {


  bottomSection(){
    return showModalBottomSheet(
      context: context,
        builder: (builder){
          return Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height/5,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10),
                  height: MediaQuery.of(context).size.height/8,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Column(
                          children: <Widget>[
                            Text('Ferme Dieu fait grace',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold
                              ),),
                            Text('Ouvert',
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: RaisedButton(
                          onPressed: (){
                             Navigator.push(
                               context,
                               MaterialPageRoute(builder: (context) => ProduitAppCartView()),
                             );
                          },
                          child: Text('VOIR',style: TextStyle(color: Colors.white),),
                          color: Color(0xfffdc229),
                        ),
                      )
                      
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Icon(Icons.call,size: 20,color: Color(0xfffdc229),),
                            Text('Appeler',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10),)
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            Icon(Icons.home,size: 20,color: Color(0xfffdc229),),
                            Text('Mes marchés',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10),)
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            Icon(Icons.shopping_basket,size: 20,color: Color(0xfffdc229),),
                            Text('Commandes',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10),)
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            Icon(Icons.people_outline,size: 20,color: Color(0xfffdc229),),
                            Text('Mon compte',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10),)
                          ],
                        ),
                      ),

                     
                    ],
                  ),
                ),
              
              ],
            ),
          );
        }
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AssetTools.colorPrimary,
        title: Text(AssetTools.appTitle),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              showSearch(context: context, delegate: DataSearch());
            },
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Image.asset(AssetTools.imageLogo),
              decoration: BoxDecoration(
                color: AssetTools.colorPrimary,
              ),
            ),
            ListTile(
              title: Text('Mes Abonnements'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            ListTile(
              title: Text('Mon historique'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            ListTile(
              title: Text('Mes favoris'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
          ],
        ),
      ),
      body: Container(
         child: new FlutterMap(
           options: new MapOptions(
             center: new LatLng(5.3204, -4.0161)),
             layers: [
               new TileLayerOptions(
                   urlTemplate:
                   "https://api.mapbox.com/styles/v1/tchirktema/cjkgokwhx1xpf2smo14ssvpmw/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoidGNoaXJrdGVtYSIsImEiOiJjazNycmZzbXMwZWJwM2VwY2VxNDljcjFwIn0.pj1tLzD7AuAIezmodQifTw",
                   additionalOptions: {
                     'accessToken':
                     'pk.eyJ1IjoidGNoaXJrdGVtYSIsImEiOiJjazNycmZzbXMwZWJwM2VwY2VxNDljcjFwIn0.pj1tLzD7AuAIezmodQifTw',
                     'id': 'mapbox.mapbox-streets-v8'
                   }),
               new MarkerLayerOptions(markers: [
                 new Marker(
                     width: 45.0,
                     height: 45.0,
                     point: new LatLng(5.307,-4.009),
                     builder: (context) => new Container(
                       child: GestureDetector(
                         onTap: (){
                           bottomSection();

                         },
                         child: Image.asset(
                           'assets/images/marker.png',
                          
                         ),
                       ),
                     )
                 ),
                 new Marker(
                     width: 45.0,
                     height: 45.0,
                     point: new LatLng(5.407,-4.009),
                     builder: (context) => new Container(
                       child: GestureDetector(
                         onTap: (){
                           bottomSection();

                         },
                         child: Image.asset(
                           'assets/images/marker.png',

                         ),
                       ),
                     )
                 ),
                 new Marker(
                     width: 45.0,
                     height: 45.0,
                     point: new LatLng(5.407,-4.109),
                     builder: (context) => new Container(
                       child: GestureDetector(
                         onTap: (){
                           bottomSection();

                         },
                         child: Image.asset(
                           'assets/images/marker.png',

                         ),
                       ),
                     )
                 ),
                 new Marker(
                     width: 45.0,
                     height: 45.0,
                     point: new LatLng(5.417,-4.129),
                     builder: (context) => new Container(
                       child: GestureDetector(
                         onTap: (){
                           bottomSection();

                         },
                         child: Image.asset(
                           'assets/images/marker.png',

                         ),
                       ),
                     )
                 )
               ])
             ]
         ),
      )
    );
  }
}