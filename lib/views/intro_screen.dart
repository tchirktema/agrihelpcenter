
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lorofe/config/asset_tools.dart';

class IntroScreen extends StatelessWidget {
  IntroScreen({Key key}) : super(key: key);

  void _onIntroEnd(context) async {
   Navigator.of(context).pushNamedAndRemoveUntil(AssetTools.rtRegistration, (Route<dynamic> route) => false);
  }

  Widget _buildImage(String assetName) {
    return Align(
      child: Image.asset(assetName, width: 350.0),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {

    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      pages: [
        PageViewModel(
          title: "COMMANDEZ EN LIGNE",
          body:
          "Achetez ce que vous voulez, quand vous le voulez : volailes, viandes ....",
          image: _buildImage(AssetTools.imageIntro3),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "RÉCUPÉREZ VOS PRODUITS",
          body:
          "Chaque semaine, la lorofe vous donne rendez-vous dans votre quartier. Venez retirer votre commande et rencontrer les Producteurs.",
          image: _buildImage(AssetTools.imageIntro4),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "MANGEZ MIEUX",
          body:
          "Locaux, frais, fermiers : découvrez et cuisinez les meilleurs produits de votre région.",
          image: _buildImage(AssetTools.imageIntro2),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "SOUTENEZ L'AGRICULTURE LOCALE",
          body:
          "Vos achats rémunèrent justement les Producteurs ...",
          image: _buildImage(AssetTools.imageIntro1),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: const Text('Passer'),
      next: const Icon(Icons.arrow_forward),
      done: const Text('Valider', style: TextStyle(fontWeight: FontWeight.w600)),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }
}
