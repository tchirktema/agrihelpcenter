import 'package:flutter/material.dart';

class Produit {
  int id;
  String name,
      desc =
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id neque libero. Donec finibus sem viverra, luctus nisi ac, semper enim. Vestibulum in mi feugiat, mattis erat suscipit, fermentum quam. Mauris non urna sed odio congue rhoncus.',
      price,
      image,
      discount;
  Color color;
  double rating;

  Produit(this.id, this.name, this.price, this.image, this.discount, this.color,
      this.rating);
}

final List<Produit> produits = [
  Produit(
      1,
      'Poulet de race',
      '1000',
      'https://upload.wikimedia.org/wikipedia/commons/5/54/Poulet_de_chair.jpg',
      '89',
      Color(0XFF558948),
      4.5),


  Produit(
      5,
      'Pintade',
      '4000',
      'https://upload.wikimedia.org/wikipedia/commons/a/a3/Pintade_domestique1.jpg',
      '75',
      Color(0XFFead04d),
      5.0),
];