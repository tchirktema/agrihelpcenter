import 'package:flutter/material.dart';


class DataSearch extends SearchDelegate<String>{
  List cities = [
    "abidjan",
    "bouaké",
    "daloa",
    "yamoussoukro",
    "san-Pédro",
    "divo",
    "korhogo",
    "anyama",
    "abengourou",
    "man",
    "gagnoa",
    "soubré",
    "agboville",
    "dabou",
    "grand-Bassam",
    "bouaflé",
    "issia",
    "sinfra",
    "katiola",
    "bingerville",
    "adzopé"
  ];


  final recentCities = [
    "abidjan",
    "bouaké",
    "daloa",
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    //  actions for appBar
    return [IconButton(icon: Icon(Icons.clear),onPressed: (){
      query = "";
    },)];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // lead
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: (){
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
  
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty?recentCities:cities.where((p)=>p.startsWith(query)).toList();
    return ListView.builder(itemBuilder: (BuildContext context,index)=> ListTile(
      onTap: (){
        Navigator.of(context).pop();
      },
      leading: Icon(Icons.location_city),
      title: RichText(text: TextSpan(
          text: suggestionList[index].substring(0,query.length),
          style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
          children: [
            TextSpan(
              text: suggestionList[index].substring(query.length),
              style: TextStyle(color: Colors.grey),
            )
          ]
        ),

      ),
    ),

     itemCount: suggestionList.length,
    );
  }

}