import 'package:flutter/material.dart';

class AssetTools {
  // string
  static const String appTitle = "Lorofe";
  static const String mapToken = 'pk.eyJ1IjoidGNoaXJrdGVtYSIsImEiOiJjazNycmZzbXMwZWJwM2VwY2VxNDljcjFwIn0.pj1tLzD7AuAIezmodQifTw';
  // route
  static const rtHomeScreen = "/homepage";
  static const rtRegistration = "/registration";

  // images
  static const String imageDirectory = "assets/images/";
  static const String imageLogo = imageDirectory+"logo.png";
  static const String imageIntro1 = imageDirectory+"1.png";
  static const String imageIntro2 = imageDirectory+"2.png";
  static const String imageIntro3 = imageDirectory+"3.png";
  static const String imageIntro4 = imageDirectory+"4.png";


  // colors
  static const Color  colorPrimary = Color(0xff69b747);
  static const Color  colorLight = Color(0xfffd9617);
  static const Color  colorDark = Color(0xff2aa450);

  static const MaterialColor colorPrimaryMaterial = const MaterialColor(0xff4e342e,
    const <int, Color>{
      50: const Color(0xff2aa450),
      100: const Color(0xff2aa450),
      200: const Color(0xff2aa450),
      300: const Color(0xff2aa450),
      400: const Color(0xff2aa450),
      500: const Color(0xff2aa450),
      600: const Color(0xff2aa450),
      700: const Color(0xff2aa450),
      800: const Color(0xff2aa450),
      900: const Color(0xff2aa450),
    },
  );


}