import 'package:flutter/material.dart';
import 'package:lorofe/config/asset_tools.dart';
import 'package:lorofe/views/auth/login_screen.dart';
import 'package:lorofe/views/handle_authentification.dart';
import 'package:lorofe/views/home/home_screen.dart';
import 'package:lorofe/views/intro_screen.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AssetTools.appTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: AssetTools.colorPrimaryMaterial,
      ),
      home: HandleAuthentification(),
      routes:<String,WidgetBuilder>{
        AssetTools.rtRegistration : (BuildContext context) => LoginScreen(),
        AssetTools.rtHomeScreen : (BuildContext context) => HomeScreen()
      }
    );
  }
}

